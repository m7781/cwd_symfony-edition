<?php

/*
 * This file is part of the PowerUI Application.
 *
 * (c)2019 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Web\Menu;

use App\Domain\User\User;
use KevinPapst\AdminLTEBundle\Event\ShowUserEvent;
use KevinPapst\AdminLTEBundle\Event\ThemeEvents;
use KevinPapst\AdminLTEBundle\Model\UserModel;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;

class NavbarUserSubscriber implements EventSubscriberInterface
{
    protected $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ThemeEvents::THEME_NAVBAR_USER => ['onShowUser', 100],
            ThemeEvents::THEME_SIDEBAR_USER => ['onShowUser', 100],
        ];
    }

    public function onShowUser(ShowUserEvent $event)
    {
        if (null === $this->security->getUser()) {
            return;
        }

        /* @var $myUser User */
        $myUser = $this->security->getUser();

        $name = (empty($myUser->getName())) ? $myUser->getUsername() : $myUser->getName();

        $user = new UserModel();
        $user
            ->setId($myUser->getId())
            ->setName($name)
            ->setUsername($myUser->getUsername())
            ->setIsOnline(true)
            ->setTitle(implode(', ', $myUser->getGroupNames())) // GROUP
            ->setAvatar(sprintf('https://www.gravatar.com/avatar/%s?s=200', md5(strtolower(trim($myUser->getEmail())))))
            ->setMemberSince($myUser->getCreatedAt())
        ;

        $event->setUser($user);
    }
}
