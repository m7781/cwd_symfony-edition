<?php

/*
 * This file is part of the PowerUI Application.
 *
 * (c)2019 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Web\Twig;

use App\Domain\User\Roles;
use Doctrine\Common\Collections\Collection;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\TwigFilter as Twig_SimpleFilter;
use Twig\Extension\AbstractExtension;

class UtilsExtension extends AbstractExtension
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getFilters()
    {
        return [
            new Twig_SimpleFilter('roleJoiner', [$this, 'formatRoles']),
            new Twig_SimpleFilter('groupJoiner', [$this, 'formatGroups']),
        ];
    }

    public function formatGroups(?Collection $groups)
    {
        if (null === $groups || 0 == $groups->count()) {
            return;
        }

        $result = [];
        /** @var Group $group */
        foreach ($groups as $group) {
            $result[] = $group->getName();
        }

        return implode(', ', $result);
    }

    public function formatRoles(?array $roles = [])
    {
        if (null === $roles) {
            return;
        }

        $values = Roles::getI18NStrings();

        $result = [];
        foreach ($roles as $role) {
            $result[] = $this->translator->trans($values[$role]);
        }

        return implode(', ', $result);
    }
}
